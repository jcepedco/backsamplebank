# Imagen raíz
FROM node

# Carpeta raíz
WORKDIR /samplebankback

# Copia de archivos
ADD . /samplebankback

# Exponer puerto
EXPOSE 3000

# Instalar dependencias
# RUN npm install

# Comando de inicialización
CMD ["npm","start"]
