var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

//variable para poder usar el request-json
var requestJson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json

var path = require('path');

var urlMlab="https://api.mlab.com/api/1/databases/samplebankbd/collections/";
var urlmovementMlab = "https://api.mlab.com/api/1/databases/samplebankbd/collections/movement";
var urlaccountMlab = "https://api.mlab.com/api/1/databases/samplebankbd/collections/account";
var urluserMlab = "https://api.mlab.com/api/1/databases/samplebankbd/collections/user";
var urlidiban = "https://api.mlab.com/api/1/databases/samplebankbd/collections/idiban";
var urliduser = "https://api.mlab.com/api/1/databases/samplebankbd/collections/iduser";

var lastUser = 0;
var lastIban = 0;
var partFixIban = 'ES010821000002000000';
var completeIban = ' ';

var mLabAPIKey = "apiKey=xAE7mMaafnYS6IQDSRH1oKnuZHkqCa8Z";
var clienteMlab = requestJson.createClient(urlaccountMlab + "?" + mLabAPIKey);
console.log(clienteMlab);

app.use(bodyParser.urlencoded({ extended : false }))
app.use(bodyParser.json())
app.listen(3000,() => {
  console.log('API REST corriendo en http://localhost:' + port)
}
)

//ALTA USUARIO: El planteamiento ha sido buscar si existe usuario, si no existe
//se da de alta usuario, cuenta, y movimiento de cuenta.

app.post('/samplebank/v1/newuser',
    function(req,res){
    var email = req.body.email;
    var password = req.body.password;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;
    var initAmount = req.body.amount;
    var query = 'q={"email":"' + email +'"}';

    httpClient = requestJson.createClient(urlMlab);
    console.log("Verificar si el cliente existe");
    console.log(urlMlab +"user?" + query + "&" + mLabAPIKey);
    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      console.log(err);
      console.log(body);
    if (err) {
      response = {"msg" : "Error obteniendo usuario."};
      res.status(500);
      res.send(response);
    } else {
      if (body.length > 0) {
          response = {"msg": "el usuario ya existe"}
          res.status(200);
          res.send(response);
      } else {
        console.log("Usuario no encontrado. Lo vamos a insertar");
        console.log("Llamamos a la tabla de ultimos ids");
        console.log(urlMlab + "lastid?" + mLabAPIKey);

        httpClient.get("lastid?" + mLabAPIKey,
        function(err1, resMlab1, body1) {
          console.log(err1);
        if (err1) {
          response = {"msg" : "Error obteniendo lastid"};
          res.status(500);
          res.send(response);
        } else {
          if (body1.length > 0) {
            lastUser = body1[0].last_user + 1;
            lastIban = body1[0].last_iban + 1;
            console.log("lastUser " + lastUser);
            console.log("lastIban " + lastIban);
            console.log("actualizamos los contadores de ids");
            var body2 = '{"last_user":'+lastUser+','+'"last_iban":'+lastIban+'}';
            console.log(body2);
            query2 = 'q={"last_user":' + body1[0].last_user +'}';
            console.log("query2"+query2);
            httpClient.put("lastid?"+query2+'&'+mLabAPIKey, JSON.parse(body2),
            function(err2,resMlab2,body2){
              if (err2){
                response = {"msg" : "Error actualizando lastid"};
                res.status(500);
                res.send(response);
              }
              else {
                console.log('Actualizado lastid')
                console.log("damos de alta el cliente en la tabla user");
                var body3 = '{"email" :' +'"'+ email +'"' +','+'"password" :' +'"' + password +'"'+ ',' + '"first_name" :' +'"'+ first_name +'"'+ ',' + '"last_name" :' + '"' + last_name +'"' + ',' + '"user_id" :' + lastUser + '}';
                console.log("body3: " + body3);
                httpClient.post("user?"+'&'+mLabAPIKey, JSON.parse(body3),
                  function(err3,resMLab3,body3){
                    if (err3) {
                      response = {"msg" : "Error actualizando lastid"};
                      res.status(500);
                      res.send(response);
                    }
                    else {
                      console.log("Informar alta de cuenta");
                      formoIban();
                      console.log('completeIban' + completeIban);
                      var body4='{"account_id": ' + lastIban + ',"account_user_id" :' + lastUser + ',"iban" : ' + '"' + completeIban + '"' + ',"balance" : ' + initAmount + '}';
                      httpClient.post("account?"+'&'+mLabAPIKey, JSON.parse(body4),
                        function(err4,resMLab4,body4){
                          if (err4) {
                            response = {"msg" : "Error insertando una nueva cuenta"};
                            res.status(500);
                            res.send(response);
                          }
                          else{
                            console.log("Informar movimiento de apertura");
                            var fechaApertura = new Date();
                            var tipMovimiento = 0; //apertura de cuenta
                            var body5='{"account_id" :'+lastIban+',"value_date" : "'+fechaApertura+'" ,"movement_type" :'+tipMovimiento+', "amount": '+initAmount+',"previous_balance": '+0+',"balance": '+initAmount +',"id_destination_account": ""}';
                            console.log("body5 " + body5);
                            httpClient.post("movement?"+'&'+mLabAPIKey, JSON.parse(body5),
                              function(err5,resMLab5,body5){
                                if (err5) {
                                  response = {"msg" : "Error insertando movimiento de apertura"};
                                  res.status(500);
                                  res.send(response);
                                }
                                else{
                                  response = {"msg" : "Alta de cliente realizada correctamente", "user_id" : lastUser, "first_name": first_name, "last_name": last_name};
                                  res.send(response);
                                }
                              }
                            );
                          }
                        }
                      );
                    }
                  }
                );

                }
            });
          } else {
            response = {"msg" : "LastId no encontrado."};
            res.status(404);
            res.send(response);
        }
        }
        })
      }
    }
    });
  }
  )

    function formoIban(){
      var n = lastIban.toString();
      completeIban = partFixIban;
      console.log('completeIban ' + completeIban);
      var i;
      for (i = 0; i < (4 -n.length); i++) {
          completeIban += '0';
          console.log('completeIban ' + completeIban);
        }
      completeIban += n;
      console.log('completeIban ' + completeIban);
    }


    app.post('/samplebank/v1/login',
      function(req,res){
      var email = req.body.email;
      var password = req.body.password;
      var query = 'q={"email":"' + email +'","password":"' + password +'"}';
      httpClient = requestJson.createClient(urlMlab);
      console.log(urlMlab +"user?" + query + "&" + mLabAPIKey);
      httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        console.log(err);
        console.log(body);
      if (err) {
        response = {"msg" : "Error obteniendo usuario."};
        res.status(500);
        res.send(response);
      } else {
        if (body.length > 0) {
            var query2='q={"user_id":'+body[0].user_id+'}';
            var putBody ='{"$set":{"logged":true}}';
            console.log("user?"+query2+'&'+mLabAPIKey);
            console.log(putBody);
            httpClient.put("user?"+query2+'&'+mLabAPIKey, JSON.parse(putBody),
            function(errPut,resMlabPut,bodyPut){
                console.log('ERROR PUT:'+errPut);
                console.log('BODY PUT:'+bodyPut);
                if (errPut) {
                  response = {"msg" : "Error actualizando login usuario"}
                  res.status(500);
                  res.send(response);
                }
                else {
                  console.log(bodyPut.length);
                  response = {"msg": "login correcto", "user_id": body[0].user_id, "first_name": body[0].first_name, "last_name": body[0].last_name};
                  res.status(200);
                  res.send(response);
                }
              });
        } else {
          response = {"msg" : "Usuario no encontrado."};
          res.status(404);
          res.send(response);
        }
   }
  })
  }
  )

  app.post('/samplebank/v1/logout',
    function(req,res){
    var user_id = req.body.user_id;
    var query = 'q={"user_id":' + user_id +'}';
    httpClient = requestJson.createClient(urlMlab);
    var putBody ='{"$unset":{"logged":""}}';
    console.log("user?"+query+'&'+mLabAPIKey);
    httpClient.put("user?"+query+'&'+mLabAPIKey, JSON.parse(putBody),
      function(errPut,resMlabPut,bodyPut){
        console.log('ERROR PUT:'+errPut);
        console.log('BODY PUT:'+bodyPut);
        if (!errPut) {
           res.status(200);
           res.send({"msg": "El usuario ha finalizado sesión", "id": user_id});
         } else {
           res.status(409);
           res.send({"msg": "Error al finalizar sesión", "id": user_id});
         }
       });
  })


  app.post('/samplebank/v1/account/movements/:a2',
    function(req,res){
      console.log("req.params.a2" + req.params.a2);
      var account_id = req.params.a2;
      var value_date = req.body.value_date;
      var movement_type = req.body.movement_type;
      var amount = req.body.amount;
      var id_destination_account = req.body.id_destination_account;
      var fecha = new Date();
      //hacemos un get a account para obtener el saldo actualizamos
      var query = 'q={"account_id":' + account_id + '}';
      httpClient = requestJson.createClient(urlMlab);
      console.log("req.params" + req.params);
      console.log(urlMlab +"account?" + query + "&" + mLabAPIKey);
      httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        console.log(err);
        console.log(body);
        console.log("consulta body.length: " + body.length);
      if (err) {
        response = {"msg" : "Error obteniendo cuenta"};
        res.status(500);
        res.send(response);
      } else {
        if (body.length > 0) {
          //hacemos una inserción de movimientos
            var previous_balance = body[0].balance;
            var post_balance = 0;
            var numamount= parseFloat(amount);
            switch (movement_type) {
              case 3: //Retirada en efectivo
              case 4: //Adeudo
              case 5: //Transferencia Realizada
                post_balance = previous_balance - numamount;
                break;
              default:
                post_balance = previous_balance + numamount;
            }

            if (post_balance < 0){
              response = {"msg" : "Cuenta en descubierto"};
              res.send(response);
            }
            else {
            var postBody = '{"account_id" :'+account_id+',"value_date" : "'+fecha+'" ,"movement_type" :'+movement_type+', "amount": '+numamount+',"previous_balance": '+previous_balance+',"balance": '+post_balance +',"id_destination_account": ""}';
            console.log("movement?"+query+'&'+mLabAPIKey);
            console.log(postBody);
            httpClient.post("movement?"+query+'&'+mLabAPIKey, JSON.parse(postBody),
            function(errPost,resMlabPost,postBody){
              if (errPost) {
                response = {"msg" : "Error insertando un nuevo movimiento"};
                res.status(500);
                res.send(response);
              }
              else{
                //hacemos una actualización del saldo en la colección account
                var queryPut = 'q={"account_id":' + account_id + '}';
                var putBody = '{"$set":{"balance":'+post_balance+'}}';
                httpClient.put("account?"+query+'&'+mLabAPIKey, JSON.parse(putBody),
                function(errPut,resMlabPut,putBody){
                  if (errPost) {
                    response = {"msg" : "Error actualizando saldo cuenta"};
                    res.status(500);
                    res.send(response);
                  }
                  else{
                    res.send(putBody);
                  }
                }
              );
              }
            }
          );
        }
        } else {
          response = {"msg" : "Cuenta no encontrada"};
          res.status(404);
          res.send(response);
     }
    }
    }
  )
  }
)

app.post('/samplebank/v2/account/movements',
  function(req,res){
    var account_id = req.body.account_id;
    var value_date = req.body.value_date;
    var movement_type = req.body.movement_type;
    var amount = req.body.amount;
    var id_destination_account = req.body.id_destination_account;
    var fecha = new Date();
    //hacemos un get a account para obtener el saldo actualizamos
    var query = 'q={"account_id":' + account_id + '}';
    httpClient = requestJson.createClient(urlMlab);
    console.log(urlMlab +"account?" + query + "&" + mLabAPIKey);
    httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      console.log(err);
      console.log(body);
      console.log("consulta body.length: " + body.length);
    if (err) {
      response = {"msg" : "Error obteniendo cuenta"};
      res.status(500);
      res.send(response);
    } else {
      if (body.length > 0) {
        //hacemos una inserción de movimientos
          var previous_balance = body[0].balance;
          var post_balance = 0;
          var numamount= parseFloat(amount);
          switch (movement_type) {
            case "3": //Retirada en efectivo
            case "4": //Adeudo
            case "5": //Transferencia Realizada
              post_balance = previous_balance - numamount;
              break;
            default:
              post_balance = previous_balance + numamount;
            }
          if (post_balance < 0){
            response = {"msg" : "Cuenta en descubierto"};
            res.status(200);
            res.send(response);
          }
          else {
          var redondeo_balance = Math.round(post_balance * 100)/100;
          post_balance = redondeo_balance;
          var postBody = '{"account_id" :'+account_id+',"value_date" : "'+fecha+'" ,"movement_type" :'+movement_type+', "amount": '+amount+',"previous_balance": '+previous_balance+',"balance": '+post_balance +',"id_destination_account": "' + id_destination_account +'"}';
          console.log("movement?"+query+'&'+mLabAPIKey);
          console.log(postBody);
          httpClient.post("movement?"+query+'&'+mLabAPIKey, JSON.parse(postBody),
          function(errPost,resMlabPost,postBody){
            if (errPost) {
              response = {"msg" : "Error insertando un nuevo movimiento"};
              res.status(500);
              res.send(response);
            }
            else{
              //hacemos una actualización del saldo en la colección account
              var queryPut = 'q={"account_id":' + account_id + '}';
              var putBody = '{"$set":{"balance":'+post_balance+'}}';
              httpClient.put("account?"+query+'&'+mLabAPIKey, JSON.parse(putBody),
              function(errPut,resMlabPut,putBody){
                if (errPost) {
                  response = {"msg" : "Error actualizando saldo cuenta"};
                  res.status(500);
                  res.send(response);
                }
                else{
                  response = {"msg": "Operación realizada correctamente"};
                  res.status(200);
                  res.send(response);
                }
              }
            );
            }
          }
        );
      }
      } else {
        response = {"msg" : "Cuenta no encontrada"};
        res.status(404);
        res.send(response);
   }
  }
  }
)
}
)
app.get('/samplebank/v1/account/movements/:a2',
  function(req,res){
    console.log("req.params.a2 " + req.params.a2);
    var account_id = req.params.a2;
    //hacemos un get a movements
    var type_movements = ["Apertura de cuenta","Ingreso de cuenta","Ingreso nómina","Retirada en efectivo","Adeudo","Transferencia realizada","Transferencia recibida"];
    var query = 'q={"account_id":' + account_id + '}';
    httpClient = requestJson.createClient(urlMlab);
    console.log(urlMlab +"movement?" + query + "&" + mLabAPIKey);
    httpClient.get("movement?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      console.log(err);
      console.log(body);
      console.log("consulta body.length: " + body.length);
    if (err) {
      response = {"msg" : "Error obteniendo movimientos"};
      res.status(500);
      res.send(response);
    } else {
        if (body.length > 0){
          var j = 0;
          var fecha = new Date;
          var fechaEsp = "";
          for (var i=0; i <body.length;i++){
            j = body[i].movement_type;
            body[i].movement_type= type_movements[j];
            fecha = new Date(body[i].value_date);
            fechaEsp = fecha.toLocaleDateString('es-ES', {formatMatcher:'basic'});
            body[i].value_date = fechaEsp;
            }
          res.status(200);
          res.send(body);
        }
        else{
          response = {"msg" : "Movimientos no encontrados"};
          res.status(404);
          res.send(response);
        }
    }
  }
  )
  }
)

app.get('/samplebank/v1/account/:user',
  function(req,res){
    console.log("req.params.a2 " + req.params.a2);
    var account_user_id = req.params.user;
    //hacemos un get a movements
    var query = 'q={"account_user_id":' + account_user_id + '}';
    httpClient = requestJson.createClient(urlMlab);
    console.log(urlMlab +"account?" + query + "&" + mLabAPIKey);
    httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      console.log(err);
      console.log(body);
      console.log("consulta body.length: " + body.length);
    if (err) {
      response = {"msg" : "Error obteniendo cuenta"};
      res.status(500);
      res.send(response);
    } else {
        if (body.length > 0){

          res.status(200);
          res.send(body);
        }
        else{
          response = {"msg" : "Cuenta no encontrada."};
          res.status(404);
          res.send(response);
        }
    }
  }
  )
  }
)

app.post('/samplebank/v1/account/:user/:initAmount',
  function(req,res){
    //accedo a la tabla auxiliar de ultimos id
    var account_user_id = req.params.user;
    var initAmount = req.params.initAmount;
    console.log(account_user_id);
    console.log(initAmount);
    console.log("consultando lastid");
    console.log(urlMlab + "lastid?" + mLabAPIKey);
    httpClient = requestJson.createClient(urlMlab);
    httpClient.get("lastid?" + mLabAPIKey,
    function(err1, resMlab1, body1) {
      console.log(err1);
    if (err1) {
      response = {"msg" : "Error obteniendo lastid"};
      res.status(500);
      res.send(response);
    } else {
      if (body1.length > 0) {
        lastUser = body1[0].last_user;
        lastIban = body1[0].last_iban + 1;
        console.log("lastUser " + lastUser);
        console.log("lastIban " + lastIban);
        console.log("actualizamos los contadores de ids");
        var body2 = '{"last_user":'+lastUser+','+'"last_iban":'+lastIban+'}';
        console.log(body2);
        query2 = 'q={"last_user":' + body1[0].last_user +'}';
        console.log("query2"+query2);
        httpClient.put("lastid?"+query2+'&'+mLabAPIKey, JSON.parse(body2),
        function(err2,resMlab2,body2){
          if (err2){
            response = {"msg" : "Error actualizando lastid"};
            res.status(500);
            res.send(response);
          }
          else {
            console.log('Actualizado lastid')
            formoIban();
            console.log('completeIban' + completeIban);
            var body4='{"account_id": ' + lastIban + ',"account_user_id" :' + lastUser + ',"iban" : ' + '"' + completeIban + '"' + ',"balance" : ' + initAmount + '}';
            httpClient.post("account?"+'&'+mLabAPIKey, JSON.parse(body4),
              function(err4,resMLab4,body4){
                if (err4) {
                  response = {"msg" : "Error insertando una nueva cuenta"};
                  res.status(500);
                  res.send(response);
                }
                else{
                  console.log("Informar movimiento de apertura");
                  var fechaApertura = new Date();
                  var tipMovimiento = 0; //apertura de cuenta
                  var body5='{"account_id" :'+lastIban+',"value_date" : "'+fechaApertura+'" ,"movement_type" :'+tipMovimiento+', "amount": '+initAmount+',"previous_balance": '+0+',"balance": '+initAmount +',"id_destination_account": ""}';
                  console.log("body5 " + body5);
                  httpClient.post("movement?"+'&'+mLabAPIKey, JSON.parse(body5),
                    function(err5,resMLab5,body5){
                      if (err5) {
                        response = {"msg" : "Error insertando movimiento de apertura"};
                        res.status(500);
                        res.send(response);
                      }
                      else{
                        response = {"msg" : "cuenta " + completeIban + " insertada correctamente"};
                        res.status(200);
                        res.send(response);
                      }
                    }
                  );

                }
              }
            );
          }
        }
      );
      }
    }
  }
);
}
)

app.post('/samplebank/v2/account/:user',
  function(req,res){
    //accedo a la tabla auxiliar de ultimos id
    var account_user_id = req.params.user;
    var initAmount = req.body.amount;
    console.log(account_user_id);
    console.log(initAmount);
    console.log("consultando lastid");
    console.log(urlMlab + "lastid?" + mLabAPIKey);
    httpClient = requestJson.createClient(urlMlab);
    httpClient.get("lastid?" + mLabAPIKey,
    function(err1, resMlab1, body1) {
      console.log(err1);
    if (err1) {
      response = {"msg" : "Error obteniendo lastid"};
      res.status(500);
      res.send(response);
    } else {
      if (body1.length > 0) {
        lastUser = body1[0].last_user;
        lastIban = body1[0].last_iban + 1;
        console.log("lastUser " + lastUser);
        console.log("lastIban " + lastIban);
        console.log("actualizamos los contadores de ids");
        var body2 = '{"last_user":'+lastUser+','+'"last_iban":'+lastIban+'}';
        console.log(body2);
        query2 = 'q={"last_user":' + body1[0].last_user +'}';
        console.log("query2"+query2);
        httpClient.put("lastid?"+query2+'&'+mLabAPIKey, JSON.parse(body2),
        function(err2,resMlab2,body2){
          if (err2){
            response = {"msg" : "Error actualizando lastid"};
            res.status(500);
            res.send(response);
          }
          else {
            console.log('Actualizado lastid')
            formoIban();
            console.log('completeIban' + completeIban);
            var body4='{"account_id": ' + lastIban + ',"account_user_id" :' + account_user_id + ',"iban" : ' + '"' + completeIban + '"' + ',"balance" : ' + initAmount + '}';
            httpClient.post("account?"+'&'+mLabAPIKey, JSON.parse(body4),
              function(err4,resMLab4,body4){
                if (err4) {
                  response = {"msg" : "Error insertando una nueva cuenta"};
                  res.status(500);
                  res.send(response);
                }
                else{
                  console.log("Informar movimiento de apertura");
                  var fechaApertura = new Date();
                  var tipMovimiento = 0; //apertura de cuenta
                  var body5='{"account_id" :'+lastIban+',"value_date" : "'+fechaApertura+'" ,"movement_type" :'+tipMovimiento+', "amount": '+initAmount+',"previous_balance": '+0+',"balance": '+initAmount +',"id_destination_account": ""}';
                  console.log("body5 " + body5);
                  httpClient.post("movement?"+'&'+mLabAPIKey, JSON.parse(body5),
                    function(err5,resMLab5,body5){
                      if (err5) {
                        response = {"msg" : "Error insertando movimiento de apertura"};
                        res.status(500);
                        res.send(response);
                      }
                      else{
                        response = {"iban" : completeIban};
                        res.status(200);
                        res.send(response);
                      }
                    }
                  );

                }
              }
            );
          }
        }
      );
      }
    }
  }
);
}
)
